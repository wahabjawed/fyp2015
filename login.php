<?php

session_start();
include 'header/connect_database.php'; 

if(isset($_GET['logout'])){
	
	if($_GET['logout'] == 'true')
	{	
		session_destroy();
	}
	
	}
	
if($_POST)
	{
	
	
	
	
	$username = $_POST['username'];
	$password = $_POST['password'];

	$query = "SELECT count(*) from user WHERE username=:username AND password =:password";
	$sth = $dbh->prepare($query);
	$sth->bindValue(':username',$username);
	$sth->bindValue(':password',$password);
	$sth->execute();
	$rows = $sth->fetch(PDO::FETCH_NUM);
	
	if($rows[0]==1)
	{
		
		$_SESSION['username'] = $username;
		header("location:index.php");
		
	}
	else
	{
		header("location:login.php?fail=true");
		
	}
	
	}
	
?>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>WSN - Panel</title>

<!-- Bootstrap Core CSS -->
<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="container">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <div class="login-panel panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Please Sign In</h3>
        </div>
        <div class="panel-body">
          <form role="form" action="login.php" method="post"> 
           <?php 
	if(isset($_GET['fail']) && $_GET['fail']=="true"){
      echo"<div class='alert alert-danger' role='alert'>
  <strong>Oh dear!</strong> Something went awry!
It seems that the ID/Password you entered were not found in our database, please try again
</div>";

}
?>
            <fieldset>
              <div class="form-group">
                <input class="form-control" placeholder="Username" name="username" type="text" autofocus>
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Password" name="password" type="password" value="">
              </div>
              <div class="checkbox">
                <label>
                  <input name="remember" type="checkbox" value="Remember Me">
                  Remember Me </label>
              </div>
              <!-- Change this to a button or input when using this as a form --> 
             <input type="submit" id="login-btn" class="btn btn-block login-btn" value="Login" />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- jQuery --> 
<script src="bower_components/jquery/dist/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script> 

<!-- Metis Menu Plugin JavaScript --> 
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="dist/js/sb-admin-2.js"></script>

</body>
</html>