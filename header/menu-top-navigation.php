<?php

echo"   <!-- Navigation -->
        <nav class='navbar navbar-default navbar-static-top' role='navigation' style='margin-bottom: 0'>
            <div class='navbar-header'>
                <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                    <span class='sr-only'>Toggle navigation</span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                </button>
                <a class='navbar-brand' href='index.php'>WSN Portal</a>
            </div>
            <!-- /.navbar-header -->

            <ul class='nav navbar-top-links navbar-right'>
                
                
                <li class='dropdown'>
                    <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                        <i class='fa fa-user fa-fw'></i>  <i class='fa fa-caret-down'></i>
                    </a>
                    <ul class='dropdown-menu dropdown-user'>
                     
                        <li><a href='login.php'><i class='fa fa-sign-out fa-fw'></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class='navbar-default sidebar' role='navigation'>
                <div class='sidebar-nav navbar-collapse'>
                    <ul class='nav' id='side-menu'>
                        
                        <li>
                            <a href='index.php'><i class='fa fa-dashboard fa-fw'></i> Dashboard</a>
                        </li>
                        <li>
                            <a href='#'><i class='fa fa-bar-chart-o fa-fw'></i>Motes Data<span class='fa arrow'></span></a>
                            <ul class='nav nav-second-level'>
                              <li>
                                    <a href='flot.php?id=1'>Mote 1</a>
                                </li>
								 <li>
                                    <a href='flot.php?id=2'>Mote 2</a>
                                </li>
								 <li>
                                    <a href='flot.php?id=3'>Mote 3</a>
                                </li>   
								<li>
                                    <a href='flot.php?id=4'>Mote 4</a>
                                </li>
								<li>
                                    <a href='flot.php?id=5'>Mote 5</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>";?>

