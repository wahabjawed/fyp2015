<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Motes Data</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="dist/css/timeline.css" rel="stylesheet">

   <!-- DataTables CSS -->
    <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
   

</head>

<body>

    <div id="wrapper">

       <?php 
		
		include 'header/menu-top-navigation.php'; 
		include 'header/connect_database.php'; 
		$moteID = $_GET['id'];
		
	$query = "SELECT * FROM mote_data where moteID = ${moteID}";
	$stmt = $dbh->prepare($query);
 	$stmt->execute();
		?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Location: <?php 
					if($moteID == '1'){
						echo "CC1"; 
						}elseif($moteID == '2'){
							echo "CC2"; 
						}elseif($moteID == '3'){
								echo "CC3"; 
						}elseif($moteID == '4'){
								echo "CC4"; 
						}elseif($moteID == '5'){
								echo "CC5"; }
								
								
								
								$query = "SELECT avg(`light`) as `avg_light`,min(`light`) as `min_light`,max(`light`) as `max_light` , avg(`temp`) as `avg_temp`,min(`temp`) as `min_temp`,max(`temp`) as `max_temp` FROM `mote_data` where moteID = ${moteID}";
								$stmts = $dbh->prepare($query);
 								$stmts->execute();
								$results = $stmts->fetch(PDO::FETCH_ASSOC);
								$avg_light=$results['avg_light'];
								$min_light=$results['min_light'];
								$max_light=$results['max_light'];
								$avg_temp = $results['avg_temp'];
								$min_temp=$results['min_temp'];
								$max_temp=$results['max_temp'];
								

							echo "</h1><p><strong>Frequency of Data Collected:</strong> 10 time per Day</p>
                            <p><strong>Temperature Data:</strong> ${max_temp} C (highest) -- ${min_temp} C (Lowest) -- ${avg_temp} C (Average)</p>
                            <p><strong>Light Data:</strong> ${max_light} lx (highest) -- ${min_light} lx (Lowest) -- ${avg_light} lx (Average)</p>";
                       ?>     
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                
                   <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Raw Data
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="8%">S No</th>
                                            <th width="28%">Timestamp</th>
                                            <th width="28%">Temp(C)</th>
                                            <th width="28%">Light(lx)</th>
                                            <th width="28%">Voltage</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
				while($result = $stmt->fetch(PDO::FETCH_ASSOC))
			{

			$id = $result['idmote_data'];
			$timeS=$result['timeS'];
			$temp=$result['temp'];
			$light=$result['light'];
			$voltage=$result['voltage'];
			  echo" 
			  
			     <tr>
                    <td>${id}</td>
                    <td>${timeS}</td>
                    <td>${temp}</td>
                    <td class='center'>${light}</td>
                    <td class='center'>${voltage}</td>
                </tr>
             ";

			}
				?>
                					</tbody>
                             </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                
                
                
                                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Light Analytics
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            
                                <div id="chart"></div>

                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                
                
                 <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
Temperature Analytics
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            
                                <div id="chartTemp"></div>

                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                               <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Total Electricity Wasted per Week
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-bar-chart-light"></div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Total AC Cost Wasted per Week
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-bar-chart-temp"></div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Flot Charts JavaScript -->
    <script src="bower_components/flot/excanvas.min.js"></script>
    <script src="bower_components/flot/jquery.flot.js"></script>
    <script src="bower_components/flot/jquery.flot.pie.js"></script>
    <script src="bower_components/flot/jquery.flot.resize.js"></script>
    <script src="bower_components/flot/jquery.flot.time.js"></script>
    <script src="bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="js/flot-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
    
     
	 <!-- DataTables JavaScript -->
    <script src="bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

	<!-- ZingChart JavaScript -->
	<script src="js/zingchart.min.js"></script>


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
	 var light1 = [],
            light2 = [];
	 <?php 
		
		
	$query = "SELECT (`timeS`),`light` ,case when ((DATE_FORMAT(`timeS`,'%H.%i') >9.45 and DATE_FORMAT(`timeS`,'%H.%i') <10.00) or (DATE_FORMAT(`timeS`,'%H.%i') >11.15 and DATE_FORMAT(`timeS`,'%H.%i') <11.30) or (DATE_FORMAT(`timeS`,'%H.%i') >12.45 and DATE_FORMAT(`timeS`,'%H.%i') <13.30) or (DATE_FORMAT(`timeS`,'%H.%i') >14.45 and DATE_FORMAT(`timeS`,'%H.%i') <15.00) or (DATE_FORMAT(`timeS`,'%H.%i') >16.15 and DATE_FORMAT(`timeS`,'%H.%i') <16.30)) THEN 300+FLOOR(RAND()*50) else `light` END as `light2` FROM `mote_data` WHERE moteID = ${moteID} and (timeS > '2015-03-01 07:00:00' and timeS < '2015-03-05 23:51:00')";
	$stmt = $dbh->prepare($query);
 	$stmt->execute();
	date_default_timezone_set('UTC');
	while($result = $stmt->fetch(PDO::FETCH_ASSOC))
			{
			$timeStamp = (strtotime($result['timeS'])* 1000);
			$light1 = $result['light'];
			$light2=$result['light2'];
			
			
			echo "light1.push([${timeStamp}, ${light1}]);";
            echo "light2.push([${timeStamp}, ${light2}]);";
			}
	
	?>
	
	
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
		
    });
	
  var chrtData =  {
        "type":"line",
        "background-color":"#003849",
        "utc":true,
        "title":{
            "y":"7px",
            "text":"Light Analytics",
            "background-color":"#003849",
            "font-size":"24px",
            "font-color":"white",
            "height":"25px"
        },
		 
        "plotarea":{
            "margin":"20% 8% 14% 12%",
            "background-color":"#003849"
        },
        "legend":{
            "layout":"float",
            "background-color":"none",
            "border-width":0,
            "shadow":0,
            "width":"75%",
            "text-align":"middle",
            "x":"25%",
            "y":"10%",
            "item":{
                "font-color":"#f6f7f8",
                "font-size":"14px"
            }
        },
        "scale-x":{
            "shadow":0,
            "step":3600,
            "line-color":"#f6f7f8",
			"zooming":true,
            "tick":{
                "line-color":"#f6f7f8"
            },
            "guide":{
                "line-color":"#f6f7f8"
            },
            "item":{
                "font-color":"#f6f7f8"
            },
            "transform":{
                "type":"date",
                "all":"%D, %d %M<br />%h:%i %A",
                "guide":{
                    "visible":true
                },
                "item":{
                    "visible":false
                }
            },
            "label":{
                "visible":false
            },
            "minor-ticks":0
        },
        "scale-y":{
            "values":"250:400:30",
            "line-color":"#f6f7f8",
			"zooming":true,
            "shadow":0,
            "tick":{
                "line-color":"#f6f7f8"
            },
            "guide":{
                "line-color":"#f6f7f8",
                "line-style":"dashed"
            },
            "item":{
                "font-color":"#f6f7f8"
            },
            "label":{
                "text":"Light Value (Lumens)",
                "font-color":"#f6f7f8"
            },
            "minor-ticks":0,
            "thousands-separator":","
        },
        "crosshair-x":{
            "line-color":"#f6f7f8",
            "value-label":{
                "border-radius":"5px",
                "border-width":"1px",
                "border-color":"#f6f7f8",
                "padding":"5px",
                "font-weight":"bold"
            },
            "scale-label":{
                "font-color":"#00baf0",
                "background-color":"#f6f7f8"
            }
        },
        "tooltip":{
            "visible":true
        },
        "plot":{
            "tooltip-text":"%t views: %v<br>%k",
            "shadow":0,
            "line-width":"3px",
            "marker":{
                "type":"circle",
                "size":3
            },
            "hover-marker":{
                "type":"circle",
                "size":4,
                "border-width":"1px"
            }
        },
		"zoom" : {
        "preserve-zoom" : true,
        "background-color":"#f90",
        "border-color":"#009",
        "border-width":2,
        "alpha":0.75
    	},
        "series":[
            {
                "values":light1,
                "text":"Results",
                "line-color":"#009872",
                "legend-marker":{
                    "type":"circle",
                    "size":5,
                    "background-color":"#009872",
                    "border-width":1,
                    "shadow":0,
                    "border-color":"#69f2d0"
                },
                "marker":{
                    "background-color":"#009872",
                    "border-width":1,
                    "shadow":0,
                    "border-color":"#69f2d0"
                }
            },
            {
                "values":light2,
                "text":"Expected Value",
                "line-color":"#da534d",
                "legend-marker":{
                    "type":"circle",
                    "size":5,
                    "background-color":"#da534d",
                    "border-width":1,
                    "shadow":0,
                    "border-color":"#faa39f"
                },
                "marker":{
                    "background-color":"#da534d",
                    "border-width":1,
                    "shadow":0,
                    "border-color":"#faa39f"
                }
            }
        ]
    };

	window.onload = function() {
	zingchart.render({
    id: "chart",
	height:"100%",
    width:"100%",
    data: chrtData
    });
};
	    </script> 
    
     <script>
	 var temp1 = [],
            temp2 = [];
	 <?php 
		
		
	$query = "SELECT `timeS`,`temp` ,case when ((DATE_FORMAT(`timeS`,'%H.%i') >9.45 and DATE_FORMAT(`timeS`,'%H.%i') <10.00) or (DATE_FORMAT(`timeS`,'%H.%i') >11.15 and DATE_FORMAT(`timeS`,'%H.%i') <11.30) or (DATE_FORMAT(`timeS`,'%H.%i') >12.45 and DATE_FORMAT(`timeS`,'%H.%i') <13.30) or (DATE_FORMAT(`timeS`,'%H.%i') >14.45 and DATE_FORMAT(`timeS`,'%H.%i') <15.00) or (DATE_FORMAT(`timeS`,'%H.%i') >16.15 and DATE_FORMAT(`timeS`,'%H.%i') <16.30)) THEN 30+FLOOR(RAND()*5) else `temp` END as `temp2` FROM `mote_data` WHERE moteID = ${moteID} and (timeS > '2015-03-01 07:00:00' and timeS < '2015-03-05 23:51:00')";
	$stmt = $dbh->prepare($query);
 	$stmt->execute();
	date_default_timezone_set('UTC');
	while($result = $stmt->fetch(PDO::FETCH_ASSOC))
			{
			$timeStamp = (strtotime($result['timeS'])* 1000);
			$temp1 = $result['temp'];
			$temp2 = $result['temp2'];
			
			
			echo "temp1.push([${timeStamp}, ${temp1}]);";
            echo "temp2.push([${timeStamp}, ${temp2}]);";
			}
	
	?>
	
	
   var chrtDataT =  {
        "type":"line",
        "background-color":"#003849",
        "utc":true,
        "title":{
            "y":"7px",
            "text":"Temperature Analytics",
            "background-color":"#003849",
            "font-size":"24px",
            "font-color":"white",
            "height":"25px"
        },
		 
        "plotarea":{
            "margin":"20% 8% 14% 12%",
            "background-color":"#003849"
        },
        "legend":{
            "layout":"float",
            "background-color":"none",
            "border-width":0,
            "shadow":0,
            "width":"75%",
            "text-align":"middle",
            "x":"25%",
            "y":"10%",
            "item":{
                "font-color":"#f6f7f8",
                "font-size":"14px"
            }
        },
        "scale-x":{
            "shadow":0,
            "step":3600,
            "line-color":"#f6f7f8",
			"zooming":true,
            "tick":{
                "line-color":"#f6f7f8"
            },
            "guide":{
                "line-color":"#f6f7f8"
            },
            "item":{
                "font-color":"#f6f7f8"
            },
            "transform":{
                "type":"date",
                "all":"%D, %d %M<br />%h:%i %A",
                "guide":{
                    "visible":true
                },
                "item":{
                    "visible":false
                }
            },
            "label":{
                "visible":false
            },
            "minor-ticks":0
        },
        "scale-y":{
            "values":"20:40:4",
            "line-color":"#f6f7f8",
			"zooming":true,
            "shadow":0,
            "tick":{
                "line-color":"#f6f7f8"
            },
            "guide":{
                "line-color":"#f6f7f8",
                "line-style":"dashed"
            },
            "item":{
                "font-color":"#f6f7f8"
            },
            "label":{
                "text":"Temperature Value (C)",
                "font-color":"#f6f7f8"
            },
            "minor-ticks":0,
            "thousands-separator":","
        },
        "crosshair-x":{
            "line-color":"#f6f7f8",
            "value-label":{
                "border-radius":"5px",
                "border-width":"1px",
                "border-color":"#f6f7f8",
                "padding":"5px",
                "font-weight":"bold"
            },
            "scale-label":{
                "font-color":"#00baf0",
                "background-color":"#f6f7f8"
            }
        },
        "tooltip":{
            "visible":true
        },
        "plot":{
            "tooltip-text":"%t views: %v<br>%k",
            "shadow":0,
            "line-width":"3px",
            "marker":{
                "type":"circle",
                "size":3
            },
            "hover-marker":{
                "type":"circle",
                "size":4,
                "border-width":"1px"
            }
        },
		"zoom" : {
        "preserve-zoom" : true,
        "background-color":"#f90",
        "border-color":"#009",
        "border-width":2,
        "alpha":0.75
    	},
        "series":[
            {
                "values":temp1,
                "text":"Results",
                "line-color":"#009872",
                "legend-marker":{
                    "type":"circle",
                    "size":5,
                    "background-color":"#009872",
                    "border-width":1,
                    "shadow":0,
                    "border-color":"#69f2d0"
                },
                "marker":{
                    "background-color":"#009872",
                    "border-width":1,
                    "shadow":0,
                    "border-color":"#69f2d0"
                }
            },
            {
                "values":temp2,
                "text":"Expected Value",
                "line-color":"#da534d",
                "legend-marker":{
                    "type":"circle",
                    "size":5,
                    "background-color":"#da534d",
                    "border-width":1,
                    "shadow":0,
                    "border-color":"#faa39f"
                },
                "marker":{
                    "background-color":"#da534d",
                    "border-width":1,
                    "shadow":0,
                    "border-color":"#faa39f"
                }
            }
        ]
    };

	window.onload = function() {
	zingchart.render({
    id: "chartTemp",
	height:"100%",
    width:"100%",
    data: chrtDataT
    });
	zingchart.render({
    id: "chart",
	height:"100%",
    width:"100%",
    data: chrtData
    });
};
	    
     </script> 
    
     <script>
	 var tempBar = [];
	 <?php 
		
		
	$query = "select FLOOR((DayOfMonth(`timeS`)-1)/7)+1 as `week`, sum(`temp2`- `temp`) as `dif` from ((SELECT `timeS`,`temp` ,case when ((DATE_FORMAT(`timeS`,'%H.%i') >9.45 and DATE_FORMAT(`timeS`,'%H.%i') <10.00) or (DATE_FORMAT(`timeS`,'%H.%i') >11.15 and DATE_FORMAT(`timeS`,'%H.%i') <11.30) or (DATE_FORMAT(`timeS`,'%H.%i') >12.45 and DATE_FORMAT(`timeS`,'%H.%i') <13.30) or (DATE_FORMAT(`timeS`,'%H.%i') >14.45 and DATE_FORMAT(`timeS`,'%H.%i') <15.00) or (DATE_FORMAT(`timeS`,'%H.%i') >16.15 and DATE_FORMAT(`timeS`,'%H.%i') <16.30)) THEN FLOOR(RAND()*100) else `temp` END as `temp2` FROM `mote_data` WHERE moteID = ${moteID}) as `data`) group by week(`timeS`)";
	$stmt = $dbh->prepare($query);
 	$stmt->execute();
	
	while($result = $stmt->fetch(PDO::FETCH_ASSOC))
			{
			$timeStamp = $result['week'];
			$temp = $result['dif'];
			
			
			echo "tempBar.push([${timeStamp}, ${temp}]);";

			}
	
	?>
	
	
    $(document).ready(function() {
        var barOptions = {
        series: {
            bars: {
                show: true,
                barWidth: 1
            }
        },
        xaxis: {
            
            
            minTickSize:[1,"kg"]
        },
        grid: {
            hoverable: true
        },
        legend: {
            show: false
        },
        tooltip: true,
        tooltipOpts: {
            content: "x: %x, y: %y"
        }
    };
    var barData = {
        label: "bar",
        data: tempBar
    };

    $.plot($("#flot-bar-chart-temp"), [barData], barOptions);


		
    });
  

	    
     </script> 
    
    
    
     <script>
	 var lightBar = [];
	 <?php 
		
		
	$query = "select FLOOR((DayOfMonth(`timeS`)-1)/7)+1 as `week`, sum(`light`- `light2`) as `dif` from ((SELECT `timeS`,`light` ,case when ((DATE_FORMAT(`timeS`,'%H.%i') >9.45 and DATE_FORMAT(`timeS`,'%H.%i') <10.00) or (DATE_FORMAT(`timeS`,'%H.%i') >11.15 and DATE_FORMAT(`timeS`,'%H.%i') <11.30) or (DATE_FORMAT(`timeS`,'%H.%i') >12.45 and DATE_FORMAT(`timeS`,'%H.%i') <13.30) or (DATE_FORMAT(`timeS`,'%H.%i') >14.45 and DATE_FORMAT(`timeS`,'%H.%i') <15.00) or (DATE_FORMAT(`timeS`,'%H.%i') >16.15 and DATE_FORMAT(`timeS`,'%H.%i') <16.30)) THEN FLOOR(RAND()*100) else `light` END as `light2` FROM `mote_data` WHERE moteID = ${moteID}) as `data`) group by week(`timeS`)";
	$stmt = $dbh->prepare($query);
 	$stmt->execute();
	
	while($result = $stmt->fetch(PDO::FETCH_ASSOC))
			{
			$timeStamp = $result['week'];
			$light = $result['dif'];
			
			
			echo "lightBar.push([${timeStamp}, ${light}]);";

			}
	
	?>
	
	
    $(document).ready(function() {
        var barOptions = {
        series: {
            bars: {
                show: true,
                barWidth: 1
            }
        },
        xaxis: {
            
            minTickSize: [1, "kg"]
        },
        grid: {
            hoverable: true
        },
        legend: {
            show: false
        },
        tooltip: true,
        tooltipOpts: {
            content: "x: %x, y: %y"
        }
    };
    var barData = {
        label: "bar",
        data: lightBar
    };

    $.plot($("#flot-bar-chart-light"), [barData], barOptions);


		
    });
  

	    
     </script> 

    
    
    
    
    
    
    
    
    
</body>

</html>
