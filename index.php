<?php

include 'header/checkloginstatus.php'; 
include 'header/connect_database.php'; 
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FYP</title>

    <!-- Bootstrap Core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
     <!-- DataTables CSS -->
    <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <?php 
		
		include 'header/menu-top-navigation.php'; 
		$query = "select * from `mote_data` where `timeS` in (SELECT max(`timeS`) FROM `mote_data` group by `moteID`) order by moteID";
					$stmts = $dbh->prepare($query);
 					$stmts->execute();
		?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                <?php 
					$results = $stmts->fetch(PDO::FETCH_ASSOC);
					$max_light=$results['light'];
					$max_temp=$results['temp'];
								
								?>
                                    <div>Temperature: <?php echo $max_temp ; ?> C</div>
                                    <div>Light: <?php echo $max_light ; ?> lx</div>
                                </div>
                            </div>
                        </div>
                        <a href="flot.php?id=1">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                        <?php 
					$results = $stmts->fetch(PDO::FETCH_ASSOC);
					$max_light=$results['light'];
					$max_temp=$results['temp'];
								
								?>
                                      <div>Temperature: <?php echo $max_temp ; ?> C</div>
                                    <div>Light: <?php echo $max_light ; ?> lx</div>
                                </div>
                            </div>
                        </div>
                        <a href="flot.php?id=2">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
						 <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                        <?php 
					$results = $stmts->fetch(PDO::FETCH_ASSOC);
					$max_light=$results['light'];
					$max_temp=$results['temp'];
								
								?>
                                   <div>Temperature: <?php echo $max_temp ; ?> C</div>
                                    <div>Light: <?php echo $max_light ; ?> lx</div>
                                </div>
                            </div>
                        </div>
                        <a href="flot.php?id=3">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                   <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                        <?php 
					$results = $stmts->fetch(PDO::FETCH_ASSOC);
					$max_light=$results['light'];
					$max_temp=$results['temp'];
								
								?>
                                   <div>Temperature: <?php echo $max_temp ; ?> C</div>
                                    <div>Light: <?php echo $max_light ; ?> lx</div>
                                </div>
                            </div>
                        </div>
                        <a href="flot.php?id=4">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
               
                   <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Raw Data
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="8%">S No</th>
                                            <th width="28%">Timestamp</th>
                                            <th width="28%">Temp(C)</th>
                                            <th width="28%">Light(lx)</th>
                                            <th width="28%">Voltage</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
									   $query = "SELECT * FROM mote_data";
	$stmt = $dbh->prepare($query);
 	$stmt->execute();
				while($result = $stmt->fetch(PDO::FETCH_ASSOC))
			{

			$id = $result['idmote_data'];
			$timeS=$result['timeS'];
			$temp=$result['temp'];
			$light=$result['light'];
			$voltage=$result['voltage'];
			  echo" 
			  
			     <tr>
                    <td>${id}</td>
                    <td>${timeS}</td>
                    <td>${temp}</td>
                    <td class='center'>${light}</td>
                    <td class='center'>${voltage}</td>
                </tr>
             ";

			}
				?>
                					</tbody>
                             </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                
                    
                       
                       
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>



   
    
    <!-- DataTables JavaScript -->
    <script src="bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
     <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
    <script>
    
	 $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
		
	});
    
    </script>


</body>

</html>
  
    </script>


</body>

</html>
