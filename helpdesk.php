<?php

include 'header/checkloginstatus.php'; 
include 'header/connect_database.php'; 
include 'header/_user-details.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<!-- Mirrored from thevectorlab.net/adminlab/blank.html by HTTrack Website Copier/3.x [XR&CO'2013], Tue, 04 Nov 2014 07:58:59 GMT -->
<head>
   <meta charset="utf-8" />
   <title>Dashboard</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="stylesheet/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="stylesheet/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="stylesheet/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
   <link href="stylesheet/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="css/style.css" rel="stylesheet" type="text/css" />
   <link href="css/style_responsive.css" rel="stylesheet" />
   <link href="css/style_default.css" rel="stylesheet" id="style_color" />

   <link href="stylesheet/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
   <link rel="stylesheet" type="text/css" href="stylesheet/uniform/css/uniform.default.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
<?php
include 'header/menu-top-navigation.php'; 
?>
      <!-- BEGIN PAGE -->  
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                  
                   <ul class="breadcrumb">
                       <li>
                           <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
                       </li>
                       <li>
                           <a href="index.php"><?php echo $full_name; ?></a> <span class="divider">&nbsp;</span>
                       </li>
                       <li>
                           <a href="#">HelpDesk</a> <span class="divider-last">&nbsp;</span>
                       </li>
                       
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="widget">
                        <div class="widget-title">
                           <h4><i class="icon-globe"></i>MUNIK HelpDesk</h4>
                                           
                        </div>
                        <div class="widget-body">
    
    
    <?php                        
	if($_POST)
	{
		$subject=$_POST['subject'];
		$message=$_POST['message'];
					
	$to = $email;
				$subject = "Munik: HelpDesk-".$subject;
				$headers = "From: reg.munik@khi.iba.edu.pk\r\n";
				$headers .= "Reply-To: ". $email . "\r\n";
				$headers .= "CC: ".$email."\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

				
				mail("reg.munik@khi.iba.edu.pk", $subject, $message, $headers);
	
	
		echo"<div class='alert alert-info' role='alert'>
  		Your ticket has been successfully generated.
  		</div>";
		
	}
	?>
     
                           If you are facing any difficulties or have some questions about registration or are curious to know more, generate a ticket to the respective department and they'll get back to you within 48 hours.
                            <br><br>
    
                           
                            
                             <form action="helpdesk.php" class="form-horizontal" method="post">
                           <div class="control-group">
                              <label class="control-label">Subject</label>
                              <div class="controls">
                                 <input type="text" class="span12 " name="subject" required/>
                                
                              </div>
                           </div>
                           
                           
                                    
                           <div class="control-group">
                              <label class="control-label">Message</label>
                              <div class="controls">
                                 <textarea row="6" class="span12 " name="message" required></textarea>
                              </div>
                           </div>
                           
                           
                           <div class="form-actions">
                              <button type="submit" class="btn btn-success">Generate New Ticket</button>
                              <button type="button" class="btn btn-danger" onClick="window.location.href='index.php';">Back</button>
                             
                           </div>
 </form>
                            
                            
                        </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
       </div>           
      
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
   <div id="footer">
       <a href ="https://www.facebook.com/avialdo.inc">2014-15 &copy; Avialdo.</a>
      <div class="span pull-right">
         <span class="go-top"><i class="icon-arrow-up"></i></span>
      </div>
   </div>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="script/jquery-1.8.3.min.js"></script>
   <script src="stylesheet/bootstrap/js/bootstrap.min.js"></script>
   <script src="script/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="stylesheet/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="stylesheet/uniform/jquery.uniform.min.js"></script>
   <script src="script/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->

<!-- Mirrored from thevectorlab.net/adminlab/blank.html by HTTrack Website Copier/3.x [XR&CO'2013], Tue, 04 Nov 2014 07:58:59 GMT -->
</html>